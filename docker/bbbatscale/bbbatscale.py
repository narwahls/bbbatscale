#!/usr/bin/env python3

import argparse
import os
import sys
from time import sleep
from typing import Optional

import django
from django.core import management
from django.core.management import CommandError
from psycopg2 import OperationalError

from BBBatScale.settings import default_settings_module


def _add_db_arguments(parser: argparse.ArgumentParser):
    parser.add_argument(
        "--db-conn-attempts",
        type=int,
        default=1,
        dest="database_connection_attempts",
        metavar="attempts",
        help="Specifies how many attempts should be made to connect to the database before aborting."
        " If the supplied number is less than one, the process will wait indefinitely"
        " until a connection could be established.",
    )
    parser.add_argument(
        "--db-conn-delay",
        type=int,
        default=5,
        dest="database_connection_delay",
        metavar="delay",
        help="Specifies the time to wait between two attempts.",
    )


def _await_database_connection(attempts: Optional[int], delay: int) -> None:
    from django.db import connections

    if attempts < 1:
        attempts = None

    attempt = 0

    print("Try to connect to database", flush=True)
    while attempts is None or attempt < attempts:
        attempt += 1
        print("Attempt", attempt, flush=True)

        try:
            for connection in connections.all():
                connection.connect()
            print("Connection successfully established", flush=True)
            return
        except OperationalError:
            pass
        finally:
            for connection in connections.all():
                connection.close()

        if attempts is None or attempt < attempts:
            sleep(delay)

    print("Could not connect to the database", file=sys.stderr, flush=True)
    sys.exit(1)


def _migrate() -> None:
    from django.core.management.commands import migrate

    try:
        management.call_command(migrate.Command(), interactive=False)
    except CommandError as exc:
        print(exc.args, file=sys.stderr, flush=True)
        sys.exit(1)


def _collect_statics(clear: bool) -> None:
    from django.contrib.staticfiles.management.commands import collectstatic

    try:
        management.call_command(collectstatic.Command(), interactive=False, clear=clear)
    except CommandError as exc:
        print(exc.args, file=sys.stderr, flush=True)
        sys.exit(1)


def _compile_messages() -> None:
    from django.core.management.commands import compilemessages

    try:
        management.call_command(compilemessages.Command())
    except CommandError as exc:
        print(exc.args, file=sys.stderr, flush=True)
        sys.exit(1)


def command_run(args: argparse.Namespace) -> None:
    import uvicorn

    _await_database_connection(args.database_connection_attempts, args.database_connection_delay)

    if args.migrate:
        _migrate()

    if args.collect_statics or args.collect_statics_clear:
        _collect_statics(args.collectstatic_clear)

    if args.compile_messages:
        _compile_messages()

    sys.exit(uvicorn.run("BBBatScale.asgi:application", host="0.0.0.0", port=8000, loop="asyncio"))


def command_await_database_connection(args: argparse.Namespace) -> None:
    sys.exit(_await_database_connection(args.database_connection_attempts, args.database_connection_delay))


def command_migrate(args: argparse.Namespace) -> None:
    _await_database_connection(args.database_connection_attempts, args.database_connection_delay)

    sys.exit(_migrate())


def command_collect_statics(args: argparse.Namespace) -> None:
    sys.exit(_collect_statics(args.clear))


def command_compile_messages(_args: argparse.Namespace) -> None:
    sys.exit(_compile_messages())


def command_collect_all_room_occupancy(_args: argparse.Namespace) -> None:
    from core.management.commands import collect_all_room_occupancy

    try:
        sys.exit(management.call_command(collect_all_room_occupancy.Command()))
    except CommandError as exc:
        print(exc.args, file=sys.stderr, flush=True)
        sys.exit(1)


def command_collect_server_stats(_args: argparse.Namespace) -> None:
    from core.management.commands import collect_server_stats

    try:
        sys.exit(management.call_command(collect_server_stats.Command()))
    except CommandError as exc:
        print(exc.args, file=sys.stderr, flush=True)
        sys.exit(1)


def command_reset_stucked_rooms(_args: argparse.Namespace) -> None:
    from core.management.commands import reset_stucked_rooms

    try:
        sys.exit(management.call_command(reset_stucked_rooms.Command()))
    except CommandError as exc:
        print(exc.args, file=sys.stderr, flush=True)
        sys.exit(1)


def command_house_keeping(_args: argparse.Namespace) -> None:
    from core.management.commands import house_keeping

    try:
        sys.exit(management.call_command(house_keeping.Command()))
    except CommandError as exc:
        print(exc.args, file=sys.stderr, flush=True)
        sys.exit(1)


def command_webhooks_worker(_args: argparse.Namespace) -> None:
    from django_rq.management.commands import rqworker

    try:
        sys.exit(management.call_command(rqworker.Command(), "webhooks", with_scheduler=True))
    except CommandError as exc:
        print(exc.args, file=sys.stderr, flush=True)
        sys.exit(1)


def command_tenant(args: argparse.Namespace) -> None:
    from core.management.commands import tenant

    try:
        sys.exit(tenant.Command().handle(**vars(args)))
    except CommandError as exc:
        print(exc.args, file=sys.stderr, flush=True)
        sys.exit(1)


if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", default_settings_module)
    django.setup()

    root_parser = argparse.ArgumentParser()
    subparsers = root_parser.add_subparsers(required=True, metavar="command")

    run_parser = subparsers.add_parser("run")
    run_parser.set_defaults(subcommand=command_run)
    _add_db_arguments(run_parser)
    run_parser.add_argument(
        "--migrate",
        action="store_true",
        help="Run migrations before proceeding.",
    )
    run_parser.add_argument(
        "--collect-statics",
        action="store_true",
        help="Collect statics before proceeding.",
    )
    run_parser.add_argument(
        "--collect-statics-clear",
        action="store_true",
        help="Clear and collect static files before proceeding.",
    )
    run_parser.add_argument(
        "--compile-messages",
        action="store_true",
        help="Compile messages before proceeding.",
    )

    await_database_connection_parser = subparsers.add_parser(
        "await-database-connection", help="Wait until the database is ready."
    )
    await_database_connection_parser.set_defaults(subcommand=command_await_database_connection)
    _add_db_arguments(await_database_connection_parser)

    from django.core.management.commands import compilemessages, migrate

    migrate_parser = subparsers.add_parser("migrate", help=migrate.Command.help)
    migrate_parser.set_defaults(subcommand=command_migrate)
    _add_db_arguments(migrate_parser)

    from core.management.commands import tenant

    tenant_parser = subparsers.add_parser("tenant", help=tenant.Command.help)
    tenant_parser.set_defaults(subcommand=command_tenant)
    tenant.add_arguments(tenant_parser)

    from django.contrib.staticfiles.management.commands import collectstatic

    collect_static_parser = subparsers.add_parser("collect-statics", help=collectstatic.Command.help)
    collect_static_parser.set_defaults(subcommand=command_collect_statics)
    collect_static_parser.add_argument(
        "--clear",
        action="store_true",
        help="Clear the directory before copying the static files.",
    )

    compile_messages_parser = subparsers.add_parser("compile-messages", help=compilemessages.Command.help)
    compile_messages_parser.set_defaults(subcommand=command_compile_messages)

    from core.management.commands import collect_all_room_occupancy

    collect_all_room_occupancy_parser = subparsers.add_parser(
        "collect-all-room-occupancy", help=collect_all_room_occupancy.Command.help
    )
    collect_all_room_occupancy_parser.set_defaults(subcommand=command_collect_all_room_occupancy)

    from core.management.commands import collect_server_stats

    collect_server_stats_parser = subparsers.add_parser("collect-server-stats", help=collect_server_stats.Command.help)
    collect_server_stats_parser.set_defaults(subcommand=command_collect_server_stats)

    from core.management.commands import reset_stucked_rooms

    reset_stucked_rooms_parser = subparsers.add_parser("reset-stucked-rooms", help=reset_stucked_rooms.Command.help)
    reset_stucked_rooms_parser.set_defaults(subcommand=command_reset_stucked_rooms)

    from core.management.commands import house_keeping

    house_keeping_parser = subparsers.add_parser("house-keeping", help=house_keeping.Command.help)
    house_keeping_parser.set_defaults(subcommand=command_house_keeping)

    from django_rq.management.commands import rqworker

    webhooks_worker_parser = subparsers.add_parser("webhooks-worker", help=rqworker.Command.help)
    webhooks_worker_parser.set_defaults(subcommand=command_webhooks_worker)

    namespace = root_parser.parse_args()
    if namespace.subcommand:
        namespace.subcommand(namespace)
    else:
        root_parser.print_help()
        root_parser.exit(1)
