# Migration Path from 2.X to 3.0

In the 3.0 release no major database change occurs.
This version introduces multi-tenancy support with django's sites framework and an oidc authentication provider for
tenant based authentication. It is tested with keycloak. For further instructions on oidc please read the 
[openIDconnect.md](openIdConnect.md).

To migrate from a running BBB@Scale v2.X version to v3.0.0, you can upgrade your project and run the migrations.
If you used the old oidc, ldap or shibboleth authentication, please have a look at [openIDconnect.md](openIdConnect.md),
since version 3 does not support them anymore. We switched to a central IAM to provide multi-tenancy authentication.



After you migrated your database you are finished.
Happy working and carry on!
