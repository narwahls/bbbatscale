import logging
from urllib.parse import urlencode

from core.views import create_view_access_logging_message
from django.contrib import auth, messages
from django.contrib.auth.decorators import user_passes_test
from django.contrib.sites.models import Site
from django.contrib.sites.shortcuts import get_current_site
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse
from django.utils.crypto import get_random_string
from django.utils.module_loading import import_string
from django.utils.translation import gettext_lazy as _
from mozilla_django_oidc.utils import absolutify, add_state_and_nonce_to_session
from mozilla_django_oidc.views import OIDCAuthenticationRequestView, OIDCLogoutView, get_next_url

from .forms import AuthParameterForm

logger = logging.getLogger(__name__)


# Create your views here.


class TenantOIDCAuthenticationRequestView(OIDCAuthenticationRequestView):
    def __init__(self, *args, **kwargs):
        pass

    def get(self, request):
        """OIDC client authentication initialization HTTP endpoint"""
        auth_parameter = get_current_site(request).auth_parameter
        state = get_random_string(auth_parameter.state_size)
        redirect_field_name = auth_parameter.redirect_field_name

        params = {
            "response_type": "code",
            "scope": auth_parameter.scopes,
            "client_id": auth_parameter.client_id,
            "redirect_uri": absolutify(request, reverse("oidc_authentication_callback")),
            "state": state,
        }

        params.update(self.get_extra_params(request))

        if auth_parameter.use_nonce:
            nonce = get_random_string(auth_parameter.nonce_size)
            params.update({"nonce": nonce})

        add_state_and_nonce_to_session(request, state, params)

        request.session["oidc_login_next"] = get_next_url(request, redirect_field_name)

        query = urlencode(params)
        redirect_url = "{url}?{query}".format(url=auth_parameter.authorization_endpoint, query=query)
        return HttpResponseRedirect(redirect_url)


class TenantOIDCLogoutView(OIDCLogoutView):
    def post(self, request):
        """Log out the user."""
        logout_url = self.redirect_url
        auth_parameter = get_current_site(request).auth_parameter
        if request.user.is_authenticated:
            # Check if a method exists to build the URL to log out the user
            # from the OP.
            logout_from_op = auth_parameter.logout_url_method
            if logout_from_op:
                logout_url = import_string(logout_from_op)(request)

            # Log out the Django user if they were logged in.
            auth.logout(request)

        return HttpResponseRedirect(logout_url)


@user_passes_test(lambda u: u.is_superuser)
def auth_parameter_create(request, tenant):
    logger.info(create_view_access_logging_message(request))
    instance = get_object_or_404(Site, pk=tenant)
    form = AuthParameterForm(request.POST or None, initial={"tenant": instance})
    if request.method == "POST" and form.is_valid():
        auth_parameter = form.save(commit=False)
        auth_parameter.tenant = instance
        auth_parameter.save()
        messages.success(request, _("AuthParameter for {} was created successfully").format(instance.name))
        # return to URL
        return redirect("tenant_overview")
    return render(request, "auth_parameter_create.html", {"form": form})


@user_passes_test(lambda u: u.is_superuser)
def auth_parameter_update(request, tenant):
    logger.info(create_view_access_logging_message(request))
    instance = get_object_or_404(Site, pk=tenant)
    form = AuthParameterForm(request.POST or None, instance=instance.auth_parameter)
    if request.method == "POST" and form.is_valid():
        form.save()
        messages.success(request, _("AuthParameter for {} was updated successfully").format(instance.name))
        # return to URL
        return redirect("tenant_overview")
    return render(request, "auth_parameter_create.html", {"form": form})


@user_passes_test(lambda u: u.is_superuser)
def auth_parameter_delete(request, tenant):
    logger.info(create_view_access_logging_message(request))
    instance = get_object_or_404(Site, pk=tenant)
    instance.auth_parameter.delete()
    messages.success(request, _("AuthParameter for {} was deleted successfully").format(instance.name))
    return redirect("tenant_overview")
