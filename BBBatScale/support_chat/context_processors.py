from typing import Dict

from django.contrib.sites.shortcuts import get_current_site
from django.http import HttpRequest
from support_chat.models import SupportChatParameter


def message_max_length(request: HttpRequest) -> Dict[str, int]:
    _tenant = get_current_site(request)
    return {"support_chat_message_max_length": SupportChatParameter.load(_tenant).message_max_length}
