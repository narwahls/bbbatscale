import pytest
from core.constants import MODERATION_MODE_MODERATORS
from core.models import Room, SchedulingStrategy, Server, User, get_default_room_config
from core.services import create_join_meeting_url


@pytest.fixture(scope="function")
def example(db) -> SchedulingStrategy:
    return SchedulingStrategy.objects.create(
        name="example",
    )


@pytest.fixture(scope="function")
def example_server(db, example) -> Server:
    return Server.objects.create(
        scheduling_strategy=example,
        dns="example.org",
        shared_secret="123456789",
    )


@pytest.fixture(scope="function")
def room(db, example, example_server) -> Room:
    return Room.objects.create(
        scheduling_strategy=example,
        server=example_server,
        name="room",
        meeting_id="12345678",
        attendee_pw="test_attendee_password",
        moderator_pw="test_moderator_password",
        moderation_mode=MODERATION_MODE_MODERATORS,
        everyone_can_start=False,
        config=get_default_room_config(),
    )


@pytest.fixture(scope="function")
def user(db) -> User:
    user = User.objects.create(username="user", email="user@example.org", is_superuser=False, is_staff=True)
    return user


def test_create_join_meeting_url(room):
    meeting_url = create_join_meeting_url(room.meeting_id, "test_username", "test_user_pw")
    expected_meeting_url = (
        "https://example.org/bigbluebutton/api/join?meetingID=12345678&password=test_user_pw&"
        "fullName=test_username&redirect=true"
        "&checksum=ac8712dea4035bab196dcf2d5f843a230ba33ca8"
    )
    assert meeting_url == expected_meeting_url
