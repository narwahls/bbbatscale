# encoding: utf-8
import logging
from datetime import timedelta

from core.models import Room
from core.services import reset_room
from core.utils import BigBlueButton
from django.core.management.base import BaseCommand
from django.db import transaction
from django.utils.timezone import now

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "perform health check on all servers"

    @transaction.non_atomic_requests
    def handle(self, *args, **options):
        logger.info("Start house_keeping command ")

        # clear monitoring values on inactive rooms
        for room in Room.objects.filter(last_running__lte=now() - timedelta(seconds=120)):
            logger.debug("Room ({}) loaded from DB to check if house cleaning is necessary".format(room))

            if not BigBlueButton.validate_is_meeting_running(
                BigBlueButton(room.server.dns, room.server.shared_secret).is_meeting_running(room.meeting_id)
            ):
                logger.debug("Room ({}) is a candidate for house keeping. Room is orphaned.")
                with transaction.atomic():
                    if room.is_breakout or room.is_externalroom():
                        room.delete()
                        logger.warning(
                            "Room ({}) was deleted. This is a indication, that BBB "
                            + "webhooks are not working properly"
                        )
                    else:
                        reset_room(room.meeting_id, room.name, room.config)
                        logger.warning(
                            "Room ({}) was reset. This is a indication, that BBB " + "webhooks are not working properly"
                        )
        logger.info("End house_keeping command")
