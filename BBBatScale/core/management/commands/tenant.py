# encoding: utf-8
import logging
from argparse import ArgumentParser
from typing import Any, Sequence, Tuple

from django.contrib.sites.models import Site
from django.core.management.base import BaseCommand, CommandError, CommandParser
from django.db.transaction import atomic

logger = logging.getLogger(__name__)


def add_arguments(parser: ArgumentParser) -> Tuple[ArgumentParser, ArgumentParser]:
    subparsers = parser.add_subparsers(required=False, metavar="command")

    parser_create = subparsers.add_parser("create", help="Create one or more tenants.")
    parser_create.add_argument(
        "--ignore-existing", action="store_true", help="Ignore already existing tenants without throwing an error."
    )
    parser_create.add_argument("tenants", nargs="+", metavar="tenant")
    parser_create.set_defaults(tenant_operation=Command.create_tenants)

    parser_rename = subparsers.add_parser("rename", help="Rename a existing tenants domain.")
    parser_rename.add_argument(
        "--overwrite",
        action="store_true",
        help="If the new_domain already exists, remove the corresponding tenant before proceeding."
        " WARNING: This will remove all entities"
        " or many2many intermediate entities relying on this tenant as well.",
    )
    parser_rename.add_argument("old_domain")
    parser_rename.add_argument("new_domain")
    parser_rename.set_defaults(tenant_operation=Command.rename_tenant)

    return parser_create, parser_rename


class Command(BaseCommand):
    help = "manage tenants"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.parser_create = None
        self.parser_rename = None

    @staticmethod
    def create_tenants(*_args: Any, tenants: Sequence[str], ignore_existing: bool, **_options: Any) -> None:
        Site.objects.bulk_create(
            map(lambda domain: Site(domain=domain, name=domain[:50]), tenants), ignore_conflicts=ignore_existing
        )

    @staticmethod
    def rename_tenant(*_args: Any, old_domain: str, new_domain: str, overwrite: bool, **_options: Any) -> None:
        with atomic():
            old_site = Site.objects.filter(domain=old_domain).first()
            if not old_site:
                raise CommandError("Old domain %s does not exist" % old_domain)

            new_site = Site.objects.filter(domain=new_domain).first()
            if new_site:
                if overwrite:
                    new_site.delete()
                else:
                    raise CommandError("New domain %s already exists" % new_domain)

            old_site.domain = new_domain
            old_site.save()

    def run_from_argv(self, argv: Sequence[str]) -> None:
        try:
            super().run_from_argv(argv)
        except CommandError as exc:
            try:
                subcommand = argv[2]
            except IndexError:
                subcommand = None

            parser: CommandParser = self.create_parser(argv[0], argv[1])

            if subcommand == "rename" and self.parser_rename:
                self.parser_rename.print_help()
            elif subcommand == "rename" and self.parser_create:
                self.parser_create.print_help()
            else:
                parser.print_help()

            if exc.args:
                print()
                print(" ".join(exc.args))
            parser.exit(2)

    def add_arguments(self, parser: CommandParser) -> None:
        self.parser_create, self.parser_rename = add_arguments(parser)

    def handle(self, *args: Any, **options: Any) -> None:
        options["tenant_operation"](*args, **options)
