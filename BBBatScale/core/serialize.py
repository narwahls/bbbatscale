import logging
from typing import Any, Dict, Iterable, List, Optional

from core.constants import MODERATION_MODE_ALL, MODERATION_MODE_MODERATORS
from core.models import GeneralParameter, HomeRoom, Meeting, Room, RoomConfiguration, SchedulingStrategy, Server, User
from django.contrib.sites.models import Site
from django.db.models import Q
from django.db.transaction import atomic

logger = logging.getLogger(__name__)


def serialize_data() -> Dict[str, Any]:
    data = dict()

    with atomic():
        logger.debug("Transaction opened; starting export")

        logger.debug("Users transformed; transaction not yet closed")

        data["room_configurations"] = list(
            {
                "name": room_configuration.name,
                "mute_on_start": room_configuration.mute_on_start,
                "moderation_mode": room_configuration.moderation_mode,
                "everyone_can_start": room_configuration.everyone_can_start,
                "authenticated_user_can_start": room_configuration.authenticated_user_can_start,
                "guest_policy": room_configuration.guest_policy,
                "allow_guest_entry": room_configuration.allow_guest_entry,
                "access_code": room_configuration.access_code,
                "only_prompt_guests_for_access_code": room_configuration.only_prompt_guests_for_access_code,
                "disable_cam": room_configuration.disable_cam,
                "disable_mic": room_configuration.disable_mic,
                "allow_recording": room_configuration.allow_recording,
                "disable_private_chat": room_configuration.disable_private_chat,
                "disable_public_chat": room_configuration.disable_public_chat,
                "disable_note": room_configuration.disable_note,
                "url": room_configuration.url,
                "maxParticipants": room_configuration.maxParticipants,
                "streamingUrl": room_configuration.streamingUrl,
            }
            for room_configuration in RoomConfiguration.objects.all()
        )
        logger.debug("Room configurations transformed; transaction not yet closed")
        data["tenants"] = list(
            {
                "name": tenant.name,
                "domain": tenant.domain,
                "users": _serialize_users(tenant.tenant_users.all()),
                "general_parameter": _serialize_general_parameter(GeneralParameter.load(tenant)),
                "scheduling_strategy": _serialize_scheduling_strategies(tenant.schedulingstrategy_set.all()),
            }
            for tenant in Site.objects.all()
        )
        logger.debug("Tenants transformed; transaction not yet closed")

        data["scheduling_strategy"] = list(
            {
                "name": scheduling_strategy.name,
                "description": scheduling_strategy.description,
                "notifications_emails": scheduling_strategy.notifications_emails,
                "token_registration": scheduling_strategy.token_registration,
                "scheduling_strategy": scheduling_strategy.scheduling_strategy,
                "servers": _serialize_servers(scheduling_strategy.server_set.all()),
                "rooms": _serialize_rooms(scheduling_strategy.room_set.all()),
            }
            for scheduling_strategy in SchedulingStrategy.objects.filter(tenants=None)
        )
        logger.debug("Scheduling Strategy (and their rooms and servers) transformed; transaction not yet closed")

        data["servers"] = _serialize_servers(Server.objects.filter(scheduling_strategy__isnull=True))
        logger.debug("Servers transformed; transaction not yet closed")

        data["rooms"] = _serialize_rooms(Room.objects.filter(scheduling_strategy__isnull=True))
        logger.debug("Rooms transformed; transaction not yet closed")

        data["homerooms"] = list(
            {
                "name": homeroom.name,
                "owner_username": homeroom.owner.username,
            }
            for homeroom in HomeRoom.objects.all()
        )
        logger.debug("Homerooms transformed; transaction not yet closed")

        data["meetings"] = list(
            {
                "room_name": meeting.room_name,
                "meeting_id": meeting.meeting_id,
                "creator": meeting.creator,
                "replay_id": meeting.replay_id,
                "started": meeting.started.isoformat(),
            }
            for meeting in Meeting.objects.all()
        )
        logger.debug("Meetings transformed; transaction not yet closed")

    logger.debug("Transaction closed")
    return data


def deserialize_data(data: Dict[str, Any]) -> Dict[str, List[str]]:
    created_tenants = list()
    updated_tenants = list()
    created_room_configurations = list()
    updated_room_configurations = list()
    created_scheduling_strategies = list()
    updated_scheduling_strategies = list()
    created_servers = list()
    updated_servers = list()
    created_rooms = list()
    updated_rooms = list()
    ignored_rooms = list()
    created_homerooms = list()
    updated_homerooms = list()
    created_meetings = list()

    with atomic():
        logger.debug("Transaction opened; starting import")

        _deserialize_room_configuration(
            data["room_configurations"],
            created_room_configurations,
            updated_room_configurations,
        )
        logger.debug("Room configurations imported; transaction not yet committed")

        for tenant in data["tenants"]:
            _tenant, created = Site.objects.update_or_create(domain=tenant["domain"], defaults={"name": tenant["name"]})
            created_tenants.append(_tenant.name) if created else updated_tenants.append(_tenant.name)

            _deserialize_users(tenant["users"], _tenant)

            _deserialize_general_parameter(tenant["general_parameter"], _tenant)

            _deserialize_scheduling_strategies(
                tenant["scheduling_strategy"],
                _tenant,
                created_scheduling_strategies,
                updated_scheduling_strategies,
                created_servers,
                updated_servers,
                created_rooms,
                updated_rooms,
                ignored_rooms,
            )
            logger.debug("Tenant imported; transaction not yet committed")

        _deserialize_scheduling_strategies(
            data["scheduling_strategy"],
            None,
            created_scheduling_strategies,
            updated_scheduling_strategies,
            created_servers,
            updated_servers,
            created_rooms,
            updated_rooms,
            ignored_rooms,
        )
        logger.debug("Scheduling Strategies imported; transaction not yet committed")

        _deserialize_servers(
            map(lambda _server: {**_server, "scheduling_strategy": None}, data["servers"]),
            created_servers,
            updated_servers,
        )
        logger.debug("Servers imported; transaction not yet committed")

        _deserialize_rooms(
            map(lambda _room: {**_room, "scheduling_strategy": None}, data["rooms"]),
            None,
            created_rooms,
            updated_rooms,
            ignored_rooms,
        )
        logger.debug("Rooms imported; transaction not yet committed")

        for homeroom in data["homerooms"]:
            room_id = Room.objects.values_list("id", flat=True).get(name=homeroom["name"])
            owner_id = User.objects.values_list("id", flat=True).get(username=homeroom["owner_username"])
            existing_homeroom: HomeRoom = HomeRoom.objects.filter(owner_id=owner_id).first()
            created = True
            name = homeroom["name"]

            if existing_homeroom is not None:
                if existing_homeroom.id == room_id:
                    continue
                created = False
                name = existing_homeroom.name + " -> " + homeroom["name"]
                existing_homeroom.delete()

            HomeRoom(room_ptr_id=room_id, owner_id=owner_id).save_base(raw=True, force_insert=True)
            created_homerooms.append(name) if created else updated_homerooms.append(name)
        logger.debug("Homerooms imported; transaction not yet committed")

        for meeting in data["meetings"]:
            _meeting = Meeting.objects.create(
                room_name=meeting["room_name"],
                meeting_id=meeting["meeting_id"],
                creator=meeting["creator"],
                replay_id=meeting["replay_id"],
            )
            _meeting.started = meeting["started"]
            _meeting.save()
            created_meetings.append(_meeting.room_name)
        logger.debug("Meetings imported; transaction not yet committed")

    logger.debug("Transaction committed")
    return {
        "created_tenants": created_tenants,
        "updated_tenants": updated_tenants,
        "created_room_configurations": created_room_configurations,
        "updated_room_configurations": updated_room_configurations,
        "created_servers": created_servers,
        "updated_servers": updated_servers,
        "created_rooms": created_rooms,
        "updated_rooms": updated_rooms,
        "ignored_rooms": ignored_rooms,
        "created_scheduling_strategies": created_scheduling_strategies,
        "updated_scheduling_strategies": updated_scheduling_strategies,
        "created_homerooms": created_homerooms,
        "updated_homerooms": updated_homerooms,
        "created_meetings": created_meetings,
        "updated_general_parameter": True,
    }


def _serialize_servers(servers: Iterable[Server]) -> List[Dict[str, Any]]:
    return list(
        {
            "dns": server.dns,
            "datacenter": server.datacenter,
            "shared_secret": server.shared_secret,
            "participant_count_max": server.participant_count_max,
            "videostream_count_max": server.videostream_count_max,
        }
        for server in servers
    )


def _deserialize_servers(
    servers: Iterable[Dict[str, Any]], created_servers: List[str], updated_servers: List[str]
) -> None:
    for server in servers:
        _server, created = Server.objects.update_or_create(
            dns=server["dns"],
            defaults={
                "scheduling_strategy": server["scheduling_strategy"],
                "datacenter": server["datacenter"],
                "shared_secret": server["shared_secret"],
                "participant_count_max": server["participant_count_max"],
                "videostream_count_max": server["videostream_count_max"],
            },
        )
        created_servers.append(_server.dns) if created else updated_servers.append(_server.dns)


def _serialize_rooms(rooms: Iterable[Room]) -> List[Dict[str, Any]]:
    return list(
        {
            "name": room.name,
            "meeting_id": room.meeting_id,
            "is_public": room.is_public,
            "comment_public": room.comment_public,
            "comment_private": room.comment_private,
            "config_name": room.config.name,
            "click_counter": room.click_counter,
            "event_collection_strategy": room.event_collection_strategy,
            "event_collection_parameters": room.event_collection_parameters,
            "maxParticipants": room.maxParticipants,
        }
        for room in rooms
    )


def _deserialize_rooms(
    rooms: Iterable[Dict[str, Any]],
    tenant: Optional[Site],
    created_rooms: List[str],
    updated_rooms: List[str],
    ignored_rooms: List[str],
) -> None:
    for room in rooms:
        _room = None

        if room["name"] not in created_rooms or room["name"] not in updated_rooms:
            if room["config_name"] is None:
                room_configuration = None
            else:
                room_configuration = RoomConfiguration.objects.get(name=room["config_name"])

            defaults = {
                "scheduling_strategy": room["scheduling_strategy"],
                "is_public": room["is_public"],
                "comment_public": room["comment_public"],
                "comment_private": room["comment_private"],
                "config": room_configuration,
                "click_counter": room["click_counter"],
                "event_collection_strategy": room["event_collection_strategy"],
                "event_collection_parameters": room["event_collection_parameters"],
            }
            if "meeting_id" in room:
                defaults["meeting_id"] = room["meeting_id"]
            if "maxParticipants" in room:
                defaults["maxParticipants"] = room["maxParticipants"]

            if Room.objects.filter(
                (Q(name=room["name"]) & ~Q(meeting_id=room["meeting_id"]))
                | (~Q(name=room["name"]) & Q(meeting_id=room["meeting_id"]))
            ).exists():
                ignored_rooms.append(room["name"])
                logger.info(
                    "Ignore room %s since a room with the same name"
                    " but another meeting ID (or vice versa) already exists" % room["name"]
                )
            else:
                _room, created = Room.objects.update_or_create(
                    name=room["name"],
                    defaults=defaults,
                )
                created_rooms.append(_room.name) if created else updated_rooms.append(_room.name)

        if tenant:
            if not _room:
                _room = Room.objects.get(name=room["name"])

            _room.tenants.add(tenant)


def _deserialize_room_configuration(
    room_configurations: Iterable[Dict[str, Any]],
    created_room_configurations: List[str],
    updated_room_configurations: List[str],
) -> None:
    for room_configuration in room_configurations:
        defaults = {
            "name": room_configuration["name"],
            "mute_on_start": room_configuration["mute_on_start"],
            "everyone_can_start": room_configuration["everyone_can_start"],
            "authenticated_user_can_start": room_configuration["authenticated_user_can_start"],
            "guest_policy": room_configuration["guest_policy"],
            "allow_guest_entry": room_configuration["allow_guest_entry"],
            # migrate old export data with `access_code_guests` to `only_prompt_guests_for_access_code`
            "access_code": room_configuration["access_code"] or room_configuration.get("access_code_guests", None),
            "only_prompt_guests_for_access_code": (
                room_configuration.get(
                    "only_prompt_guests_for_access_code",
                    bool(not room_configuration["access_code"] and room_configuration.get("access_code_guests", None)),
                )
            ),
            "disable_cam": room_configuration["disable_cam"],
            "disable_mic": room_configuration["disable_mic"],
            "allow_recording": room_configuration["allow_recording"],
            "disable_private_chat": room_configuration["disable_private_chat"],
            "disable_public_chat": room_configuration["disable_public_chat"],
            "disable_note": room_configuration["disable_note"],
            "url": room_configuration["url"],
            "streamingUrl": room_configuration["streamingUrl"],
        }
        if "maxParticipants" in room_configuration:
            defaults["maxParticipants"] = room_configuration["maxParticipants"]

        try:
            defaults["moderation_mode"] = room_configuration["moderation_mode"]
        except KeyError as exc:
            if "all_moderator" in room_configuration:
                logger.warning(
                    "The RoomConfiguration '%s' contains the legacy attribute"
                    " 'all_moderater', please use 'moderation_mode' instead"
                )
                defaults["moderation_mode"] = (
                    MODERATION_MODE_ALL if room_configuration["all_moderator"] else MODERATION_MODE_MODERATORS
                )
            else:
                raise exc

        _room_configuration, created = RoomConfiguration.objects.update_or_create(
            name=room_configuration["name"],
            defaults=defaults,
        )
        created_room_configurations.append(_room_configuration.name) if created else updated_room_configurations.append(
            _room_configuration.name
        )


def _serialize_users(users: Iterable[User]) -> List[Dict[str, Any]]:
    return list(
        {
            "username": user.username,
            "email": user.email,
            "password": user.password,
            "first_name": user.first_name,
            "last_name": user.last_name,
            "is_active": user.is_active,
            "is_staff": user.is_staff,
            "is_superuser": user.is_superuser,
            "last_login": user.last_login.isoformat() if user.last_login is not None else None,
            "date_joined": user.date_joined.isoformat(),
        }
        for user in users
    )


def _deserialize_users(users: Iterable[Dict[str, Any]], tenant: Site) -> None:
    for user in users:
        User.objects.update_or_create(
            username=user["username"],
            defaults={
                "email": user["email"],
                "password": user["password"],
                "first_name": user["first_name"],
                "last_name": user["last_name"],
                "is_active": user["is_active"],
                "is_staff": user["is_staff"],
                "is_superuser": user["is_superuser"],
                "last_login": user["last_login"],
                "date_joined": user["date_joined"],
                "tenant": tenant,
            },
        )


def _serialize_general_parameter(general_parameter: GeneralParameter) -> Dict[str, Any]:
    return {
        "faq_url": general_parameter.faq_url,
        "favicon_link": general_parameter.favicon_link,
        "feedback_email": general_parameter.feedback_email,
        "footer": general_parameter.footer,
        "home_room_enabled": general_parameter.home_room_enabled,
        "home_room_room_configuration_name": general_parameter.home_room_room_configuration.name
        if general_parameter.home_room_room_configuration is not None
        else None,
        "home_room_teachers_only": general_parameter.home_room_teachers_only,
        "home_room_scheduling_strategy_name": general_parameter.home_room_scheduling_strategy.name
        if general_parameter.home_room_scheduling_strategy is not None
        else None,
        "jitsi_url": general_parameter.jitsi_url,
        "latest_news": general_parameter.latest_news,
        "logo_link": general_parameter.logo_link,
        "page_title": general_parameter.page_title,
        "participant_total_max": general_parameter.participant_total_max,
        "playback_url": general_parameter.playback_url,
        "download_url": general_parameter.download_url,
    }


def _deserialize_general_parameter(general_parameter: Dict[str, Any], tenant: Site) -> None:
    _general_parameter = GeneralParameter.load(tenant)
    _general_parameter.faq_url = general_parameter["faq_url"]
    _general_parameter.favicon_link = general_parameter["favicon_link"]
    _general_parameter.feedback_email = general_parameter["feedback_email"]
    _general_parameter.footer = general_parameter["footer"]
    _general_parameter.home_room_enabled = general_parameter["home_room_enabled"]
    _general_parameter.home_room_room_configuration = (
        RoomConfiguration.objects.get(name=general_parameter["home_room_room_configuration_name"])
        if general_parameter["home_room_room_configuration_name"] is not None
        else None
    )
    _general_parameter.home_room_teachers_only = general_parameter["home_room_teachers_only"]
    _general_parameter.home_room_scheduling_strategy = (
        SchedulingStrategy.objects.get(name=general_parameter["home_room_scheduling_strategy_name"])
        if general_parameter["home_room_scheduling_strategy_name"] is not None
        else None
    )
    _general_parameter.jitsi_url = general_parameter["jitsi_url"]
    _general_parameter.latest_news = general_parameter["latest_news"]
    _general_parameter.logo_link = general_parameter["logo_link"]
    _general_parameter.page_title = general_parameter["page_title"]
    _general_parameter.participant_total_max = general_parameter["participant_total_max"]
    _general_parameter.playback_url = general_parameter["playback_url"]
    _general_parameter.download_url = general_parameter["download_url"]
    _general_parameter.save()


def _serialize_scheduling_strategies(scheduling_strategies: Iterable[SchedulingStrategy]) -> List[Dict[str, Any]]:
    return list(
        {
            "name": scheduling_strategy.name,
            "description": scheduling_strategy.description,
            "notifications_emails": scheduling_strategy.notifications_emails,
            "token_registration": scheduling_strategy.token_registration,
            "scheduling_strategy": scheduling_strategy.scheduling_strategy,
            "servers": _serialize_servers(scheduling_strategy.server_set.all()),
            "rooms": _serialize_rooms(scheduling_strategy.room_set.all()),
        }
        for scheduling_strategy in scheduling_strategies
    )


def _deserialize_scheduling_strategies(
    scheduling_strategies: Iterable[Dict[str, Any]],
    tenant: Optional[Site],
    created_scheduling_strategies: List[str],
    updated_scheduling_strategies: List[str],
    created_servers: List[str],
    updated_servers: List[str],
    created_rooms: List[str],
    updated_rooms: List[str],
    ignored_rooms: List[str],
) -> None:
    for scheduling_strategy in scheduling_strategies:
        _scheduling_strategy = None

        if (
            scheduling_strategy["name"] not in created_scheduling_strategies
            or scheduling_strategy["name"] not in updated_scheduling_strategies
        ):
            _scheduling_strategy, created = SchedulingStrategy.objects.update_or_create(
                name=scheduling_strategy["name"],
                defaults={
                    "description": scheduling_strategy["description"],
                    "notifications_emails": scheduling_strategy["notifications_emails"],
                    "token_registration": scheduling_strategy["token_registration"],
                    "scheduling_strategy": scheduling_strategy["scheduling_strategy"],
                },
            )
            created_scheduling_strategies.append(
                _scheduling_strategy.name
            ) if created else updated_scheduling_strategies.append(_scheduling_strategy.name)

            _deserialize_servers(
                map(
                    lambda _server: {**_server, "scheduling_strategy": _scheduling_strategy},
                    scheduling_strategy["servers"],
                ),
                created_servers,
                updated_servers,
            )

            _deserialize_rooms(
                map(lambda _room: {**_room, "scheduling_strategy": _scheduling_strategy}, scheduling_strategy["rooms"]),
                tenant,
                created_rooms,
                updated_rooms,
                ignored_rooms,
            )

        if tenant:
            if not _scheduling_strategy:
                _scheduling_strategy = SchedulingStrategy.objects.get(name=scheduling_strategy["name"])

            _scheduling_strategy.tenants.add(tenant)
