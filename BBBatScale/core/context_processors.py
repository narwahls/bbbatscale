from core.models import GeneralParameter
from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site
from django.http import HttpRequest


def general_parameter(request: HttpRequest):
    _tenant = get_current_site(request)
    return {"general_parameter": GeneralParameter.load(_tenant)}


def oidc(request: HttpRequest):
    return {"oidc_enabled": settings.OIDC_ENABLED}


def auth_urls(request: HttpRequest):
    return {"login_url": str(settings.LOGIN_URL), "logout_url": str(settings.LOGOUT_URL)}
