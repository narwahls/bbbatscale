import logging

from core.models import Meeting, Room, SchedulingStrategy, Server
from core.views import create_view_access_logging_message
from django.contrib.sites.shortcuts import get_current_site
from django.shortcuts import render

logger = logging.getLogger(__name__)


def statistics(request):
    logger.info(create_view_access_logging_message(request))
    tenant = get_current_site(request)
    return render(
        request,
        "statistics.html",
        {
            "servers": Server.objects.filter(scheduling_strategy__tenants__in=[tenant]),
            "rooms": Room.objects.filter(tenants__in=[tenant]),
            "participants_current": Room.get_participants_current(tenant),
            "performed_meetings": Meeting.objects.all().count(),
            "scheduling_strategies": SchedulingStrategy.objects.filter(tenants__in=[tenant]),
        },
    )
