import hmac
import logging
from urllib.parse import quote

from core.constants import ROOM_STATE_ACTIVE, ROOM_STATE_CREATING, ROOM_STATE_INACTIVE
from core.forms import ConfigureRoomForm
from core.models import GeneralParameter, Room
from core.services import (
    create_join_meeting_url,
    get_join_password,
    join_or_create_meeting_permissions,
    meeting_create,
    room_click,
)
from core.views import create_view_access_logging_message
from django.conf import settings
from django.contrib import messages
from django.contrib.sites.shortcuts import get_current_site
from django.db.transaction import atomic, non_atomic_requests
from django.http import HttpResponse, HttpResponseNotAllowed, JsonResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)


def join_redirect(request, room):
    logger.info(create_view_access_logging_message(request))

    try:
        pk, meeting_id = Room.objects.values_list("pk", "meeting_id").get(name=room)
        room_click(pk)
        return redirect("join_or_create_meeting", meeting_id)
    except Room.DoesNotExist:
        messages.error(request, _("Unable to find Room with given name: '{}'.").format(room))
        return redirect("home")


def join_or_create_meeting(request, meeting_id):
    logger.info(create_view_access_logging_message(request, meeting_id))

    _room = get_object_or_404(Room, meeting_id=meeting_id)
    if not request.user.is_authenticated:
        if not _room.allow_guest_entry:
            messages.warning(
                request, _("The meeting is not allowed for guest entry. Please login to access the meeting.")
            )
            logger.debug(
                "user is not authenticated and not room.allow_guest_entry; 'The meeting"
                " is not allowed for guest entry. Please login to access the meeting.' redirect to 'login'"
            )
            return redirect(settings.LOGIN_URL + "?next=" + quote(request.path_info, safe=""))

        if "username" not in request.session:
            logger.debug(
                "user is not authenticated and 'username' not in request.session; redirect to set_username_pre_join"
            )
            return redirect("set_username_pre_join", meeting_id)

    if _room.state == ROOM_STATE_INACTIVE and join_or_create_meeting_permissions(request.user, _room):
        logger.debug(
            "room.state is INACTIVE and permissions for user=(%s) are sufficient; redirect to 'create_meeting'"
        )
        return redirect("create_meeting", _room.meeting_id)

    logger.debug("Redirect to 'join_meeting'")
    return redirect("join_meeting", _room.meeting_id)


@non_atomic_requests
def create_meeting(request, meeting_id):
    logger.info(create_view_access_logging_message(request, meeting_id))

    logger.debug("Query Database Object and lock it for further requests!")
    instance = get_object_or_404(Room, meeting_id=meeting_id)
    general_parameter = GeneralParameter.load(get_current_site(request))
    if not request.user.is_authenticated and "username" not in request.session:
        logger.debug(
            "user is not authenticated and username is not in request.session; "
            + "redirect to 'set_username_pre_join', meeting_id=%s",
            meeting_id,
        )
        return redirect("set_username_pre_join", meeting_id)

    if not join_or_create_meeting_permissions(request.user, instance):
        logger.debug("user has no join_or_create_meeting_permissions; redirect to 'join_meeting'")
        return redirect("join_meeting", instance.meeting_id)

    form = ConfigureRoomForm(
        request.POST or None, instance=instance, enable_recordings=general_parameter.enable_recordings
    )
    if request.user.is_authenticated:
        creator_name = request.user.username
    else:
        creator_name = request.session["username"]
    logger.debug("creator_name set to %s", creator_name)

    if request.method == "POST" and form.is_valid():
        _room = form.save(commit=False)
        with atomic():
            logger.debug("Start transactions for room creation")
            if (
                Room.objects.filter(meeting_id=meeting_id, state=ROOM_STATE_INACTIVE).update(
                    state=ROOM_STATE_CREATING, participant_count=1, last_running=now()
                )
                >= 1
            ):
                meeting_create(
                    request,
                    {
                        "name": instance.name,
                        "meeting_id": instance.meeting_id,
                        "attendee_pw": instance.attendee_pw,
                        "moderator_pw": instance.moderator_pw,
                        "mute_on_start": _room.mute_on_start,
                        "moderation_mode": _room.moderation_mode,
                        "everyone_can_start": _room.everyone_can_start,
                        "access_code": _room.access_code,
                        "only_prompt_guests_for_access_code": _room.only_prompt_guests_for_access_code,
                        "disable_cam": _room.disable_cam,
                        "disable_mic": _room.disable_mic,
                        "disable_note": _room.disable_note,
                        "disable_private_chat": _room.disable_private_chat,
                        "disable_public_chat": _room.disable_public_chat,
                        "allow_recording": _room.allow_recording if general_parameter.enable_recordings else False,
                        "guest_policy": _room.guest_policy,
                        "url": _room.url,
                        "creator": creator_name,
                        "allow_guest_entry": _room.allow_guest_entry,
                        "logoutUrl": _room.logoutUrl,
                        "dialNumber": _room.dialNumber,
                        "welcome_message": _room.welcome_message,
                        "maxParticipants": _room.maxParticipants,
                        "streamingUrl": _room.streamingUrl,
                    },
                )
                return redirect("join_meeting", _room.meeting_id)
            else:
                messages.warning(
                    request,
                    _(
                        "The meeting was started by another moderator."
                        " You will be redirected to the meeting shortly."
                    ),
                )
                logger.debug(
                    "Meeting was started by another moderator in parallel; " + "redirect to 'join_or_create_meeting'"
                )

                return redirect("join_or_create_meeting", instance.meeting_id)

    logger.debug("Redirect to configure_room_for_meeting due to invalid form")
    return render(request, "configure_room_for_meeting.html", {"form": form, "room": instance})


def join_meeting(request, meeting_id):
    logger.info(create_view_access_logging_message(request, meeting_id))

    _room = get_object_or_404(Room, meeting_id=meeting_id)
    user_needs_access_code = _room.access_code and (
        not _room.only_prompt_guests_for_access_code or request.user.is_anonymous
    )

    if request.user.is_anonymous:
        if not _room.allow_guest_entry:
            messages.warning(request, _("The moderator did not allow guest entry. Please login to access the meeting."))
            logger.debug(
                "The moderator did not allow guest entry. Please login to access the meeting; redirect to 'login'"
            )
            return redirect(settings.LOGIN_URL + "?next=" + quote(request.path_info, safe=""))
        elif "username" not in request.session:
            return redirect("join_or_create_meeting", _room.meeting_id)

    if _room.state == ROOM_STATE_ACTIVE:
        logger.debug("Room.state is ACTIVE")
        join_name = str(request.user) if request.user.is_authenticated else request.session["username"]
        password = get_join_password(
            request.user,
            _room,
            request.user.username if request.user.is_authenticated else join_name,
        )
        logger.debug("join_name=%s; password=%s", join_name, password)

        if user_needs_access_code:
            access_code = request.POST.get("access_code", None)
            if access_code:
                if hmac.compare_digest(access_code.encode("UTF-8"), _room.access_code.encode("UTF-8")):
                    request.session.pop("username", None)
                    return redirect(create_join_meeting_url(_room.meeting_id, join_name, password))
                else:
                    messages.error(request, _("Incorrect access code."))
        else:
            request.session.pop("username", None)
            return redirect(create_join_meeting_url(_room.meeting_id, join_name, password))

    # Meeting is not running or access code is needed
    context = {"room": _room, "demand_access_code": user_needs_access_code and _room.state == ROOM_STATE_ACTIVE}
    return render(request, "join_meeting.html", context)


def session_set_username(request):
    logger.info(create_view_access_logging_message(request))

    if request.method != "POST":
        return HttpResponseNotAllowed(["POST"])
    request.session["username"] = request.POST.get("username")
    return HttpResponse("ok")


def session_set_username_pre_join(request, meeting_id):
    logger.info(create_view_access_logging_message(request, meeting_id))

    if "username" in request.session:
        return redirect("join_or_create_meeting", meeting_id)
    return render(request, "set_username.html", {"meeting_id": meeting_id})


def get_meeting_status(request, meeting_id):
    logger.info(create_view_access_logging_message(request, meeting_id))

    _room = get_object_or_404(Room, meeting_id=meeting_id)
    logger.debug("Was asked for state of {}. Answer is {}".format(_room, _room.state))
    return JsonResponse({"is_running": _room.state == ROOM_STATE_ACTIVE})
