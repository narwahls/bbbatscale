from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
from django.conf import settings
from django.urls import path


class LifespanApp:
    """
    Temporary shim for https://github.com/django/channels/issues/1216#issuecomment-678333723.
    Needed so that nginx unit doesn't display an error.
    This uses ASGI 2.0 format, not the newer 3.0 single callable.
    """

    def __init__(self, scope):
        self.scope = scope

    async def __call__(self, receive, send):
        if self.scope["type"] == "lifespan":
            while True:
                message = await receive()
                if message["type"] == "lifespan.startup":
                    await send({"type": "lifespan.startup.complete"})
                elif message["type"] == "lifespan.shutdown":
                    await send({"type": "lifespan.shutdown.complete"})
                    return


application_mapping = {"lifespan": LifespanApp}

websocket_urlpatterns = []
websocket_urlrouter = None

if settings.SUPPORT_CHAT_ENABLED:
    import support_chat.routing

    websocket_urlpatterns.append(path("supportchat/", URLRouter(support_chat.routing.websocket_urlpatterns)))

if websocket_urlpatterns:
    websocket_urlrouter = URLRouter([path("ws/", URLRouter(websocket_urlpatterns))])
    application_mapping["websocket"] = AuthMiddlewareStack(websocket_urlrouter)

application = ProtocolTypeRouter(application_mapping)
