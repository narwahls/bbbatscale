import os

os.environ.setdefault("SUPPORT_CHAT", "enabled")
os.environ.setdefault("WEBHOOKS", "enabled")
os.environ.setdefault("OIDC", "enabled")

from .development import *  # noqa: F401,F403,E402
